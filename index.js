"use strict";
const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const fs = require("fs");
var multer = require("multer");
const bodyParser = require("body-parser");
const { IamAuthenticator } = require("ibm-watson/auth");
const path = require("path");
const os = require("os");
const QRReader = require("qrcode-reader");

const jimp = require("jimp");
const { Console } = require("console");

const { from, of } = require("rxjs");
const { map, tap, switchMap, delay } = require("rxjs/operators");

const mainPath = __dirname;

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

class AppServer {
    start = 0;
    text = "";
    constructor() {
        this.start = Date.now();
        this.ConfigureEndPoints();
        this.Run();
    }

    GetIPAddress() {
        var interfaces = os.networkInterfaces();
        for (var devName in interfaces) {
            var iface = interfaces[devName];

            for (var i = 0; i < iface.length; i++) {
                var alias = iface[i];
                if (
                    alias.family === "IPv4" &&
                    alias.address !== "127.0.0.1" &&
                    !alias.internal
                )
                    return alias.address;
            }
        }
        return "0.0.0.0";
    }

    ConfigureEndPoints() {
        const self = this;
        app.get("/test", function (req, res) {
            res.send("test");
        });

        app.get("/user/:id", function (req, res) {
            res.send("user: " + req.params.id);
        });
    }

    Run() {
        const localIP = this.GetIPAddress();

        http.listen(3000, function () {
            console.log("Listening on " + localIP + ":3000");
        });
    }
}

var serv = new AppServer();
