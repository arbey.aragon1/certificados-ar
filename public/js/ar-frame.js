let parent = null;
let urlCoffe = "";
let urlConference = "";

window.addEventListener("message", ({ data, source }) => {
    if (parent === null) {
        parent = source;
    }

    console.log("/*/*/*/*/*/*/*/");
    console.log(data);
    const payload = JSON.parse(data);
    urlCoffe = payload.urlCoffe;
    urlConference = payload.urlConference;
    console.log(payload);

    document.getElementById("scene").style.display = "block";

    var entityEl = document.querySelector("#name");
    entityEl.setAttribute("value", payload.fullName);
});

AFRAME.registerComponent("button-events", {
    init: function () {
        console.log("button-events");
    },
});
