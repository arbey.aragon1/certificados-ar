const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const id = urlParams.get("id");

var url_btn1 = "";
var url_btn2 = "";

document.querySelector("#name").setAttribute("value", id);

var payload = {};

if (id === null || id == "") {
    var arr = window.location.href.split("/");
    var result = arr[0] + "//" + arr[2];
    window.location.href = result;
} else {
    var firebaseConfig = {
        apiKey: "AIzaSyBL8nnf4uO2ZoENHwmvG0BvHLlnaJB3iK8",
        authDomain: "certificados-afa13.firebaseapp.com",
        databaseURL: "https://certificados-afa13.firebaseio.com",
        projectId: "certificados-afa13",
        storageBucket: "certificados-afa13.appspot.com",
        messagingSenderId: "500106892999",
        appId: "1:500106892999:web:42b2a6eea0420849be10e2",
        measurementId: "G-416E2HRKSB",
    };

    firebase.initializeApp(firebaseConfig);
    const db = firebase.database();
    db.ref("certificates/" + id)
        .once("value")
        .then(function (snapshot) {
            const val = snapshot.val();
            if (val == null) {
                var arr = window.location.href.split("/");
                var result = arr[0] + "//" + arr[2];
                window.location.href = result;
            } else {
                payload = {
                    fullName: val.fullName,
                    urlConference: val.videoConference,
                    urlCoffe: val.videCoffe,
                    urlDay1: val.dayOne,
                    urlDay2: val.dayTwo,
                };

                if (id.includes("id")) {
                    if (payload.urlCoffe == null || payload.urlCoffe == "") {
                        document
                            .querySelector("#btn1")
                            .setAttribute("visible", false);
                    } else {
                        url_btn1 = payload.urlCoffe;
                        document
                            .querySelector("#btnText1")
                            .setAttribute("value", "Coffee");
                    }

                    if (
                        payload.urlConference == null ||
                        payload.urlConference == ""
                    ) {
                        document
                            .querySelector("#btn2")
                            .setAttribute("visible", false);
                    } else {
                        url_btn2 = payload.urlConference;
                        document
                            .querySelector("#btnText2")
                            .setAttribute("value", "Presentation");
                    }
                } else if (id.includes("as") || id.includes("sp")) {
                    if (payload.urlDay1 == null || payload.urlDay1 == "") {
                        document
                            .querySelector("#btn1")
                            .setAttribute("visible", false);
                    } else {
                        url_btn1 = payload.urlDay1;
                        document
                            .querySelector("#btnText1")
                            .setAttribute("value", "Day one");
                    }

                    if (payload.urlDay2 == null || payload.urlDay2 == "") {
                        document
                            .querySelector("#btn2")
                            .setAttribute("visible", false);
                    } else {
                        url_btn2 = payload.urlDay2;
                        document
                            .querySelector("#btnText2")
                            .setAttribute("value", "Day two");
                    }
                }
                if (id.includes("id")) {
                    document
                        .querySelector("#back")
                        .setAttribute("material", "src: #cert2;");
                } else if (id.includes("as")) {
                    document
                        .querySelector("#back")
                        .setAttribute("material", "src: #cert1;");
                } else if (id.includes("sp")) {
                    document
                        .querySelector("#back")
                        .setAttribute("material", "src: #cert3;");
                }
                document
                    .querySelector("#name")
                    .setAttribute("value", payload.fullName);
            }
        });
}

function btn1() {
    console.log(url_btn1);
    window.location.href = url_btn1;
}

function btn2() {
    console.log(url_btn2);
    window.location.href = url_btn2;
}
