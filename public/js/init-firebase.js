var firebaseConfig = {
    apiKey: "AIzaSyBL8nnf4uO2ZoENHwmvG0BvHLlnaJB3iK8",
    authDomain: "certificados-afa13.firebaseapp.com",
    databaseURL: "https://certificados-afa13.firebaseio.com",
    projectId: "certificados-afa13",
    storageBucket: "certificados-afa13.appspot.com",
    messagingSenderId: "500106892999",
    appId: "1:500106892999:web:42b2a6eea0420849be10e2",
    measurementId: "G-416E2HRKSB",
};
/*
firebase.initializeApp(firebaseConfig);
const db = firebase.database();
/** */
let iWindow = null;
let parent = null;

const iframe = document.getElementById("inner");
const formData = document.getElementById("formData");

function infoConfirmed() {
    const text = document.getElementById("idField").value;
    if (iWindow === null || text == "") {
        return;
    }

    const payload = {
        fullName: "Arbey",
        urlConference: "https://youtu.be/z22PsVfZJCc",
        urlCoffe: "https://youtu.be/njUg-5dmFys",
    };

    iWindow.postMessage(JSON.stringify(payload));
    formData.style.display = "none";
    iframe.style.height = "800px";
    /** */
    /*db.ref("certificates/" + text)
        .once("value")
        .then(function (snapshot) {
            const val = snapshot.val();
            console.log("**************");
            console.log(val);
            //var entityEl = document.querySelector("#name");
            //entityEl.setAttribute("value", val.name);
            const payload = {
                fullName: val.fullName,
                urlConference: val.videoConference,
                urlCoffe: val.videCoffe,
            };
            iWindow.postMessage(JSON.stringify(payload));
            formData.style.display = "none";
            iframe.style.height = "800px";
        });/** */
}

window.addEventListener("message", (event) => {
    const { data } = event;
    console.log(data);
    window.location.href = data;
});

iframe.addEventListener("load", () => {
    iWindow = iframe.contentWindow;
});
